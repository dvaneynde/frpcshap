﻿using NUnit.Framework;
using System;
namespace FRPcsharp
{
    public class FrpTestClass
    {
        /// <summary>
        /// s0 := number
        /// s1 := s0() * 5
        /// </summary>
        [Test()]
        public void SimpleTest()
        {
            Signal s0 = new Signal("s0", () => 5);
            Signal s1 = new Signal("s1", () => (int)s0.Apply() * 5);
            Assert.AreEqual(5, (int)s0.Apply());
            Assert.AreEqual(25, (int)s1.Apply());

            s0.Update(() => 8);
            Assert.AreEqual(8, (int)s0.Apply());
            Assert.AreEqual(40, (int)s1.Apply());

            s1.Update(() => (int)s0.Apply() / 2);
            Assert.AreEqual(8, (int)s0.Apply());
            Assert.AreEqual(4, (int)s1.Apply());
        }

        /// <summary>
        /// a,b := number
        /// s := switch, boolean
        /// z := if (s) then a else b
        /// </summary>
        [Test()]
        public void ObserversTest()
        {
            Signal a = new Signal("a", () => 5);
            Signal b = new Signal("b", () => 7);
            Signal s = new Signal("s", () => true);
            Signal z = new Signal("z", () => (bool)s.Apply() ? (int)a.Apply() : (int)b.Apply());
            Assert.AreEqual(5, (int)z.Apply());
            Assert.AreEqual(5, (int)a.Apply());
            Assert.AreEqual(7, (int)b.Apply());
            Console.WriteLine($"s=true: a={a}");
            Console.WriteLine($"s=true: b={b}");

            s.Update(() => false);
            a.Update(() => 3);
            b.Update(() => 11);
            Assert.AreEqual(11, (int)z.Apply());
            Assert.AreEqual(3, (int)a.Apply());
            Assert.AreEqual(11, (int)b.Apply());
            Console.WriteLine($"s=false: a={a}");
            Console.WriteLine($"s=false: b={b}");

        }

        /// <summary>
        /// a := value
        /// b := a() * 2
        /// s := boolean switch
        /// m := if (s()) then b:= a() else b:= a()*3 
        /// </summary>
        [Test()]
        public void UpdateTest()
        {
            Signal a = new Signal("a", () => 3);
            Signal b = new Signal("b", () => (int)a.Apply() * 2);
            Assert.AreEqual(3, (int)a.Apply());
            Assert.AreEqual(6, (int)b.Apply());
            Signal s = new Signal("s", () => true);
            Signal m = new Signal("m", () => { if ((bool)s.Apply()) { b.Update(() => (int)a.Apply()); return 0; } else { b.Update(() => (int)a.Apply() * 3); return 0; } });
            Assert.AreEqual(3, (int)b.Apply());
            
            s.Update(() => false);
            Assert.AreEqual(9, (int)b.Apply());
        }

    }
}
