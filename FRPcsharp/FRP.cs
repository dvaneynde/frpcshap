﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace FRPcsharp
{

    interface WithApply
    {
        object Apply();
    }

    public class Signal : WithApply
    {

        internal static readonly StackableVariable caller = new StackableVariable();

        private String name;
        private Func<object> expression;
        private object currentValue;
        private HashSet<Signal> observers;

        public Signal(String name, Func<object> expression)
        {
            this.name = name;
            observers = new HashSet<Signal>();
            Update(expression);
        }

        public void Update(Func<object> newExpresion)
        {
            this.expression = newExpresion;
            ComputeValue();
        }

        protected void ComputeValue()
        {
            object newValue = caller.WithValue(this, expression);
            if (currentValue == null || !currentValue.Equals(newValue))
            {
                currentValue = newValue;
                List<object> currentObservers = new List<object>(observers);
                observers.Clear();
                foreach (Signal s in currentObservers)
                    s.ComputeValue();
            }
        }

        public object Apply()
        {
            var o = (Signal) caller.Value();
            if (o != null)
                observers.Add(o);
            return currentValue;
        }

        public override string ToString()
        {
            var observersString = String.Join(",", observers.ToList().Select(s => s.name));
            return $"Signal {name} observers={observersString}";
        }
    }

    class StackableVariable
    {
        private LinkedList<WithApply> values = new LinkedList<WithApply>();

        public WithApply Value() { return values.First?.Value; }

        public R WithValue<R>(WithApply newValue, Func<R> operationToPerform)
        {
            values.AddFirst(new LinkedListNode<WithApply>(newValue));
            R result = operationToPerform();
            values.RemoveFirst();
            return result;
        }
    }

    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Initially");
            Signal s0 = new Signal("a", () => 5);
            Signal s1 = new Signal("s1", () => (int)s0.Apply() * 5);
            Console.WriteLine("s0.val=" + s0.Apply());
            Console.WriteLine("s1.val=" + s1.Apply());

            Console.WriteLine("Change value s0");
            s0.Update(() => 8);
            Console.WriteLine("s0.val=" + s0.Apply());
            Console.WriteLine("s1.val=" + s1.Apply());

            Console.WriteLine("Updated formula s1");
            s1.Update(() => (int)s0.Apply() / 2);
            Console.WriteLine("s0.val=" + s0.Apply());
            Console.WriteLine("s1.val=" + s1.Apply());
        }
    }
}
